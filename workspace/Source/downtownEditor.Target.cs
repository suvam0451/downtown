// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;
using System.IO;

public class downtownEditorTarget : TargetRules
{
	public downtownEditorTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Editor;
		bLegacyPublicIncludePaths = false;
		ShadowVariableWarningLevel = WarningLevel.Error;
		// PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		ExtraModuleNames.AddRange( new string[] { "downtown" } );
	}
}
