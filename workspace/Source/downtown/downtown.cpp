// Fill out your copyright notice in the Description page of Project Settings.

#include "downtown.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"
#include "Logging/LogMacros.h"
#include "Misc/Paths.h"
#include "Runtime/Launch/Resources/Version.h"
#include "Runtime/RenderCore/Public/ShaderCore.h"

void FMainModule::StartupModule()
{
#if (ENGINE_MINOR_VERSION > 21)    
    AddShaderSourceDirectoryMapping(TEXT("/Project"), FPaths::Combine(*(FPaths::ProjectDir()), TEXT("Shaders")));
#endif
}

void FMainModule::ShutdownModule()
{
}


IMPLEMENT_PRIMARY_GAME_MODULE( FMainModule, downtown, "downtown" );
