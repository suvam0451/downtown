// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UCustomComp.h"
#include "Modules/ModuleManager.h"
#include "Misc/Paths.h"
#include "Runtime/Launch/Resources/Version.h"
#include "Runtime/RenderCore/Public/ShaderCore.h"

#define LOCTEXT_NAMESPACE "FCustomCompModule"

void FCustomCompModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	#if (ENGINE_MINOR_VERSION > 21)    
    	AddShaderSourceDirectoryMapping(TEXT("/WW"), FPaths::Combine(*(FPaths::ProjectPluginsDir()), TEXT("CustomComp/Shaders")));
	#endif
}

void FCustomCompModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCustomCompModule, CustomComp)