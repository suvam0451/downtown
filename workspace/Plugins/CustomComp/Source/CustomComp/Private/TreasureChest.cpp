// Fill out your copyright notice in the Description page of Project Settings.


#include "TreasureChest.h"

// Sets default values
ATreasureChest::ATreasureChest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;
	SceneRoot->SetMobility(EComponentMobility::Type::Static);
	
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(SceneRoot);

	treasureBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Treasure Base"));
	treasureBase->SetupAttachment(SceneRoot);
}

// Called when the game starts or when spawned
void ATreasureChest::BeginPlay()
{
	Super::BeginPlay();
	OpenChest();
}

void ATreasureChest::OpenChest(){
	TreasureDel.BindUFunction(this, FName("rotateout"), 0.5f);
	GetWorldTimerManager().SetTimer(TreasureTimer, TreasureDel, 0.016f, true);
}
void ATreasureChest::CloseChest(){
		TreasureDel.BindUFunction(this, FName("rotatein"), 0.5f);
	GetWorldTimerManager().SetTimer(TreasureTimer, TreasureDel, 0.016f, true);
}

void ATreasureChest::rotateout(float val){
	if(mesh->GetComponentRotation().Roll < -rotationOffset){
		GetWorld()->GetTimerManager().ClearTimer(TreasureTimer);
		TreasureDel.BindUFunction(this, FName("rotatein"), rotationSpeed);
		GetWorldTimerManager().SetTimer(TreasureTimer, TreasureDel, 0.016f, true);
	}
	else{
		mesh->AddLocalRotation(FRotator(0.0f,0.0f,-val));
	}
}
void ATreasureChest::rotatein(float val){
	if(mesh->GetComponentRotation().Roll > 0.0f){
		GetWorld()->GetTimerManager().ClearTimer(TreasureTimer);
		TreasureDel.BindUFunction(this, FName("rotateout"), rotationSpeed);
		GetWorldTimerManager().SetTimer(TreasureTimer, TreasureDel, 0.016f, true);
	}
	else{
		mesh->AddLocalRotation(FRotator(0.0f,0.0f,val));
	}
}