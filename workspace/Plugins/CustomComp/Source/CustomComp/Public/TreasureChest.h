// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "TreasureChest.generated.h"

UCLASS()
class CUSTOMCOMP_API ATreasureChest : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATreasureChest();
	UPROPERTY()
		USceneComponent* SceneRoot;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* treasureBase;
	UPROPERTY(EditAnywhere, Category = "Param")
		float rotationOffset = 45.0f;
	UPROPERTY(EditAnywhere, Category = "Param")
		float rotationSpeed = 0.5f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void OpenChest();
	void CloseChest();
	UFUNCTION()
		void rotateout(float val);
	UFUNCTION()
		void rotatein(float val);

	FTimerHandle TreasureTimer;
	FTimerDelegate TreasureDel;
};
