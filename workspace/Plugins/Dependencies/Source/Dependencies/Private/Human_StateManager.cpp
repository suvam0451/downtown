// Fill out your copyright notice in the Description page of Project Settings.


#include "Human_StateManager.h"
#include "WW_Player.h"

UHuman_StateManager::UHuman_StateManager() {
	
}

UHuman_StateManager::UHuman_StateManager(EPlayerStates Horizontal, EPlayerStates Vertical)
{
	HorizontalState = Horizontal;
	VerticalState = Vertical;
}

void UHuman_StateManager::TRequest_Hori(EPlayerStates b)
{
	switch (HorizontalState) {
	case EPlayerStates::Walking: {FromWalking(b); break; }
	case EPlayerStates::Grabbing: {FromGrabbing(b); break; }
	case EPlayerStates::Running: {FromRunning(b); break; }
	case EPlayerStates::Flying: {FromFlying(b); break; }
	default: {break; }
	}
}
void UHuman_StateManager::TRequest_Vert(EPlayerStates b) {
	switch (VerticalState) {
	case EPlayerStates::Jumping: {FromJumping(b); break; }
	case EPlayerStates::Falling: {FromFalling(b); break; }
	case EPlayerStates::Hanging: {FromHanging(b); break; }
	case EPlayerStates::Grounded: {FromGrounded(b); break; }
	case EPlayerStates::Floating: {FromFloating(b); break; }
	default: {break; }
	}
}

void UHuman_StateManager::FromWalking(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Walking: {break; }
	case EPlayerStates::Grabbing: {break; }
	case EPlayerStates::Running: {break; }
	case EPlayerStates::Flying: {break; }
	default: {break; }
	}
}

void UHuman_StateManager::FromGrabbing(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Walking: {break; }
	case EPlayerStates::Grabbing: {break; }
	case EPlayerStates::Running: {break; }
	case EPlayerStates::Flying: {break; }
	default: {break; }
	}
}

void UHuman_StateManager::FromRunning(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Walking: { break; }
	case EPlayerStates::Grabbing: {break; }
	case EPlayerStates::Running: {break; }
	case EPlayerStates::Flying: {break; }
	default: {break; }
	}
}

void UHuman_StateManager::FromFlying(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Walking: { break; }
	case EPlayerStates::Grabbing: {break; }
	case EPlayerStates::Running: {break; }
	case EPlayerStates::Flying: {break; }
	default: {break; }
	}
}

void UHuman_StateManager::FromJumping(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Jumping: { break; }
	case EPlayerStates::Falling: { break; }
	case EPlayerStates::Hanging: { break; }
	case EPlayerStates::Grounded: { break; }
	case EPlayerStates::Floating: { break; }
	}
}
void UHuman_StateManager::FromFalling(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Jumping: { break; }
	case EPlayerStates::Falling: { break; }
	case EPlayerStates::Hanging: { break; }
	case EPlayerStates::Grounded: { break; }
	case EPlayerStates::Floating: { break; }
	}
}
void UHuman_StateManager::FromHanging(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Jumping: { break; }
	case EPlayerStates::Falling: { break; }
	case EPlayerStates::Hanging: { break; }
	case EPlayerStates::Grounded: { break; }
	case EPlayerStates::Floating: { break; }
	}
}
void UHuman_StateManager::FromGrounded(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Jumping: { UE_LOG(LogTemp, Warning, TEXT("Trying to jump")); bOnGround ? (PlayerRef->LaunchCharacter(FVector(0.0f, 0.0f, jumpHeight), false, true)) : true; break; }
	case EPlayerStates::Falling: { break; }
	case EPlayerStates::Hanging: { break; }
	case EPlayerStates::Grounded: { break; }
	case EPlayerStates::Floating: { break; }
	}
}
void UHuman_StateManager::FromFloating(EPlayerStates in)
{
	switch (in) {
	case EPlayerStates::Jumping: { break; }
	case EPlayerStates::Falling: { break; }
	case EPlayerStates::Hanging: { break; }
	case EPlayerStates::Grounded: { break; }
	case EPlayerStates::Floating: { break; }
	}
}