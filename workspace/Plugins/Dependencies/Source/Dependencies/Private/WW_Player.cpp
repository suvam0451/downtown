// Fill out your copyright notice in the Description page of Project Settings.

#include "WW_Player.h"
//#include "CustomUI/Public/CameraShake01.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Dependencies/Public/Libraries/StaticLib.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AWW_Player::AWW_Player()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CharacterCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("camera"));
	//CharacterCameraComponent.SetupAttachment(RootComponent);
	CharacterCameraComponent->SetupAttachment(GetCapsuleComponent());
	CharacterCameraComponent->bUsePawnControlRotation = true;

	PlayerMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	PlayerMesh->SetOnlyOwnerSee(true);
	PlayerMesh->SetupAttachment(CharacterCameraComponent);
	PlayerMesh->bCastDynamicShadow = false;
	PlayerMesh->CastShadow = false;
	PlayerMesh->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	PlayerMesh->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

// Called when the game starts or when spawned
void AWW_Player::BeginPlay()
{
	Super::BeginPlay();
	//StateManagerRef = 
	StateManagerRef = NewObject<UHuman_StateManager>();
	StateManagerRef->VerticalState = EPlayerStates::Grounded;
	StateManagerRef->HorizontalState = EPlayerStates::Walking;
	StateManagerRef->bOnGround = true;
	StateManagerRef->PlayerRef = this;
	

	//StateManagerRef
	horizontalState = EPlayerStates::Walking;
	verticalState = EPlayerStates::Grounded;

	// Gets a reference got all uses in class
	ControllerRef = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	//One liner if you want to do in current scope
	APlayerController* example = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

void AWW_Player::OnConstruction(const FTransform& Transform)
{
	GetCapsuleComponent()->InitCapsuleSize(CapsuleRadius, CapsuleHeight);
	GetCapsuleComponent()->SetCapsuleSize(CapsuleRadius, CapsuleHeight);

	CharacterCameraComponent->SetRelativeLocation(CameraRelativeLocation); // Position the camera
}

// Called every frame
void AWW_Player::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AWW_Player::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	// set up gameplay key bindings
	check(InputComp);

	InputComp->BindAction("Sprint", IE_Pressed, this, &AWW_Player::ActionMapper<EPlayerActions::Sprint,1>);
	InputComp->BindAction("Sprint", IE_Released, this, &AWW_Player::ActionMapper<EPlayerActions::Sprint,0>);
	InputComp->BindAction("Jump", IE_Pressed, this, &AWW_Player::ActionMapper<EPlayerActions::Jump, 1>);
	InputComp->BindAction("Jump", IE_Released, this, &AWW_Player::ActionMapper<EPlayerActions::Jump, 0>);

	InputComp->BindAxis("MoveForward", this, &AWW_Player::AxisMapper<1>);
	InputComp->BindAxis("MoveRight", this, &AWW_Player::AxisMapper<2>);
	InputComp->BindAxis("Turn", this, &AWW_Player::AxisMapper<3>);
	InputComp->BindAxis("LookUp", this, &AWW_Player::AxisMapper<4>);
}

template<EPlayerActions actionName, bool isPressed>
void AWW_Player::ActionMapper()
{
	//---Mapping key presses to functions after DLL query...
	switch (actionName) {
	case EPlayerActions::Sprint: { isPressed ? ReactionMapper(EResponses::StartSprint) : ReactionMapper(EResponses::StopSprint); break; }
	case EPlayerActions::Jump: { isPressed ? StateManagerRef->TRequest_Vert(EPlayerStates::Jumping) : true; break; }
	default: { break; }
	}
}

template<int32 axisName>
void AWW_Player::AxisMapper(float Value)
{
	switch (axisName) {
	case 1: { AddMovementInput(GetActorForwardVector(), Value); break; }
	case 2: { AddMovementInput(GetActorRightVector(), Value); break; }
	case 3: { APawn::AddControllerYawInput(Value); break; }
	case 4: { APawn::AddControllerPitchInput(Value); break; }
	default: {break; }
	}
}

void AWW_Player::ReactionMapper(EResponses in) {
	switch(in) {
	case EResponses::DoNothing : {return; }
	case EResponses::StartSprint : {
		FString str = "action sprinting mapping";
		GetCharacterMovement()->MaxWalkSpeed = jogSpeed;
		CameraShakeTimerMap(1, true);
		CameraShakeMap(1, true);
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, FString::Printf(TEXT("%s"), *(str)));
		break;
	}
	case EResponses::StopSprint: {
		GetCharacterMovement()->MaxWalkSpeed = walkSpeed;
		CameraShakeTimerMap(1, false);
		break;
	}
	case EResponses::StartDashing: {
		
	}
	case EResponses::DashNotAvailable: {
		
	}
	default: {}
	}
}

//Sets up and clear timers
void AWW_Player::CameraShakeTimerMap(int Index, bool Timerkey)
{
	switch (Index) {
	case(1): {
		if (Timerkey) {
			CameraShake_TDelegate.BindUFunction(this, FName("CameraShakeMap"), 1, true);
			GetWorldTimerManager().SetTimer(CameraShake_THandle, CameraShake_TDelegate, 1.5f, true);
			return;
		}
		else {
			GetWorld()->GetTimerManager().ClearTimer(CameraShake_THandle);
		}
		break;
	}
	default: {break; }
	}
}

void AWW_Player::CameraShakeMap(int Index, bool key)
{
	switch (Index) {
	case 1: {
		if (key) {
			//ControllerRef->ClientPlayCameraShake(UWW_PlayerCameraShake::StaticClass(), 1.0f, ECameraAnimPlaySpace::CameraLocal);
		}
		else {
			//ControllerRef->ClientStopCameraShake(UWW_PlayerCameraShake::StaticClass(), true);
			GetWorld()->GetTimerManager().ClearTimer(CameraShake_THandle);
		}
		break;
	}
	}
}