// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UDependencies.h"

#define LOCTEXT_NAMESPACE "FDependencies"

void FDependencies::StartupModule(){}
void FDependencies::ShutdownModule(){}

IMPLEMENT_MODULE(FDependencies, Dependencies)
#undef LOCTEXT_NAMESPACE