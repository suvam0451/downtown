// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "AssetRegistry/Private/AssetRegistry.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Runtime/AssetRegistry/Public/AssetRegistryModule.h"
#include "AssetMngr.generated.h"

UCLASS()
class DEPENDENCIES_API UAssetMngr : public UObject
{
	GENERATED_BODY()
public:
	// UAssetMngr();
	void konichiwa();

	/*	@ Looks up data table in location by name.
		@ Tests rows against given index
		@ returns asset list using 'Scriptables' default pipeline
		- Returns false on fail with error log. Please stop execution on fail.
		- Returns true on success
		*/
	template<class any, class filter>
	bool DT_LookUpAndTest(FString location, FString filename, UDataTable* &output, TArray<FAssetData> &_folderAssetData, any* &PresetRow, int CompareTo, bool useFilter = false)
	{
		FAssetRegistryModule* AssetRegistryModule = &FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		IAssetRegistry& AssetRegistry = AssetRegistryModule->Get();
		TArray<FAssetData> RefList;
		FARFilter Filter;
		Filter.bRecursivePaths = false;
		Filter.PackagePaths.Add(FName(*location));
		Filter.ClassNames.Add(UDataTable::StaticClass()->GetFName());
		AssetRegistry.GetAssets(Filter, RefList);

		for (auto it : RefList) {
			if (it.AssetName.ToString() == filename) {
				output = it.IsValid() ? Cast<UDataTable>(it.GetAsset()) : nullptr;
				 //_transitoryList;
				TArray<FName> _transitoryList = output ? output->GetRowNames() : TArray<FName, FDefaultAllocator>();
				if (CompareTo >= _transitoryList.Num()) { 
					UE_LOG(LogTemp, Warning, TEXT("Index parameter exceeds available presets for %s"), *filename);
					return false; 
				}
				else {
					PresetRow = output->FindRow<any>(_transitoryList[CompareTo], "", true);
					if (PresetRow == nullptr) { 
						UE_LOG(LogTemp, Warning, TEXT("Row structure broken for scriptable %s Asset"), *filename);  
						return false; }
					if (useFilter) {
						FileListByClass<filter>(location + *"/" + _transitoryList[CompareTo].ToString(), _folderAssetData);
						return true;
					}
					else {
						
					}
					return true;
				}
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("Preset table named %s not found at specified location."), *filename);
		return false;
	}

	template<class any>
	void FileListByClass(FString location, TArray<FAssetData> &in)
	{
		FAssetRegistryModule* AssetRegistryModule = &FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		IAssetRegistry& AssetRegistry = AssetRegistryModule->Get();
		FARFilter Filter;
		Filter.bRecursivePaths = false;
		Filter.PackagePaths.Add(FName(*location));
		//Filter.ClassNames.Add(any::StaticClass()->GetFName());
		Filter.ClassNames.Add(UStaticMesh::StaticClass()->GetFName());
		AssetRegistry.GetAssets(Filter, in);
		return;
	}

	template<class any>
	void FetchAndAssign(TMap<int, any*>& ISM_Map, TMap<int, FString> table, FString path) 
	{
		TArray<FAssetData> meshData;
		FileListByClass<UStaticMesh>(path, meshData);
		for (int i = 0; i < meshData.Num(); i++) {
			for (int j = 0; j < table.Num(); j++) {
				if (meshData[i].AssetName.ToString() == table[j]) { 
					ISM_Map[j]->SetStaticMesh(Cast<UStaticMesh>(meshData[i].GetAsset()));
				}
			}
		}
		return;
	}
};