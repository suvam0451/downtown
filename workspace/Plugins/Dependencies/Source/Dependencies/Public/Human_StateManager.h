// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Human_StateManager.generated.h"

UENUM(BlueprintType)
enum class EPlayerActions : uint8
{
	Sprint UMETA(DisplayName = "Sprint Action"),
	Jump UMETA(DisplayName = "Jump Action"),
};

UENUM(BlueprintType)
enum class EPlayerStates : uint8
{
	//Horizontal and Vertical movements to be used separately
	//Included in same enum to reduce codebase...

	//Horizontal movement components
	Walking UMETA(DisplayName = "Walking"),
	Grabbing UMETA(DisplayName = "Grabbing"),
	Running UMETA(DisplayName = "Running"),
	Flying UMETA(DisplayName = "Flying"),

	//Vertical movement components
	Jumping UMETA(DisplayName = "Jumping"),
	Falling UMETA(DisplayName = "Falling"),
	Hanging UMETA(DisplayName = "Hanging"),
	Grounded UMETA(DisplayName = "Grounded"),
	Floating UMETA(DisplayName = "Floating")
};

UENUM(BlueprintType)
enum class EResponses : uint8
{
	DoNothing,
	StartSprint,
	StopSprint,
	Jump,
	StartDashing,
	DashNotAvailable
};

UCLASS()
class DEPENDENCIES_API UHuman_StateManager : public UObject
{
	GENERATED_BODY()
public:
	UHuman_StateManager();
	UHuman_StateManager(EPlayerStates Horizontal, EPlayerStates Vertical);
	
	class AWW_Player* PlayerRef;
	void TRequest_Hori(EPlayerStates b);
	void TRequest_Vert(EPlayerStates b);
	EPlayerStates HorizontalState;
	EPlayerStates VerticalState;
	bool bOnGround;

	UPROPERTY(EditAnywhere)
		float jumpHeight = 500.0f;

	void FromWalking(EPlayerStates in);
	void FromGrabbing(EPlayerStates in);
	void FromRunning(EPlayerStates in);
	void FromFlying(EPlayerStates in);

	void FromJumping(EPlayerStates in);
	void FromFalling(EPlayerStates in);
	void FromHanging(EPlayerStates in);
	void FromGrounded(EPlayerStates in);
	void FromFloating(EPlayerStates in);
};
