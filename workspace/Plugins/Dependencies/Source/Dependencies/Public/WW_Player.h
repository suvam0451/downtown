// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "TimerManager.h"
#include "Dependencies/Public/Human_StateManager.h"
#include "WW_Player.generated.h"

UCLASS()
class DEPENDENCIES_API AWW_Player : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWW_Player();
	UPROPERTY(EditAnywhere, Category = "Param")
		USkeletalMeshComponent* PlayerMesh;
	UPROPERTY(EditAnywhere, Category = "Param")
		UCameraComponent* CharacterCameraComponent;

	UPROPERTY(EditAnywhere, Category = "Param")
		float CapsuleHeight = 90.0f;
	UPROPERTY(EditAnywhere, Category = "Param")
		float CapsuleRadius = 30.0f;
	UPROPERTY(EditAnywhere, Category = "Param")
		FVector CameraRelativeLocation = FVector(-39.56f, 1.75f, 64.f);
protected:
	EPlayerStates horizontalState, verticalState;
	FTimerHandle CamerablendSpace_THandle;
	FTimerDelegate CamerablendSpace_TDelegate;
private:
	APlayerController* ControllerRef;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UHuman_StateManager* StateManagerRef;

	/**
	 * Called when an instance of this class is placed (in editor) or spawned.
	 * @param	Transform			The transform the actor was constructed at.
	*/
	virtual void OnConstruction(const FTransform& Transform) override;
public:

	template<EPlayerActions actionName, bool isPressed>
	void ActionMapper();

	template<int32 axisName>
	void AxisMapper(float Value);

	void ReactionMapper(EResponses in);

	UPROPERTY()
		bool bCanJump = false;
	UPROPERTY()
		bool bCanWalk = false;
	UPROPERTY()
		bool bCanDash = false;

	//parameters
	UPROPERTY()
		float walkSpeed = 400.0f;
	UPROPERTY()
		float jogSpeed = 600.0f;
	UPROPERTY()
		float runSpeed = 1000.0f;
	UPROPERTY()
		float gravityScale = 1.5f;
	UPROPERTY()
		float dashDistance = 1000.0f;
	UPROPERTY()
		float dashDuration = 1.5f;

	//
	UPROPERTY()
		bool dashAvailable = true;
	UPROPERTY()
		bool bOnGround = true;

	FTimerHandle CameraShake_THandle;
	FTimerDelegate CameraShake_TDelegate;

	UFUNCTION()
		void CameraShakeTimerMap(int Index, bool timerKey);
	UFUNCTION()
		void CameraShakeMap(int Index, bool key);
};
