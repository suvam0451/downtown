// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
//#include "CustomUI/Public/UI/MainInterface.h"
#include "Managers/AssetMngr.h"
#include "ThirdP.generated.h"

UCLASS()
class DEPENDENCIES_API AThirdP : public AGameModeBase
{
	GENERATED_BODY()
public:
	AThirdP();
	UUserWidget* MainWidget;
	UUserWidget* CurrentWidget;
	UAssetMngr* AssetManager;
	TSubclassOf<UUserWidget> MainWidgetClass;
private:
	virtual void BeginPlay() override;
	void Seasons();
};
