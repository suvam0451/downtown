// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "DevSettings.generated.h"

/**
 * 
 */
UCLASS()
class DEPENDENCIES_API UDevSettings : public UDeveloperSettings
{
	GENERATED_BODY()
	
};
