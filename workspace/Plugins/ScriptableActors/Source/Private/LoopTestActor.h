// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Core/Public/Async/ParallelFor.h"
#include "LoopTestActor.generated.h"

UCLASS()
class ALoopTestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALoopTestActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

class FPrimeSearchTask : public FNonAbandonableTask
{
	friend class FAutoDeleteAsyncTask<FPrimeSearchTask>;

public:
	FPrimeSearchTask(int32 Input, int32 Input2): MyInput(Input), MyInput2(Input2){

	}

protected:
	int32 MyInput;
	int32 MyInput2;

	void DoWork() {
		// Work to be pasted here
		// Called as follow
		// (new FAutoDeleteAsyncTask<FPrimeSearchTask>(6.10))->StartBackgroundTask();
	}

	// Absolutely no idea why this should be here...
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FMyTaskName, STATGROUP_ThreadPoolAsyncTasks);
	}
};