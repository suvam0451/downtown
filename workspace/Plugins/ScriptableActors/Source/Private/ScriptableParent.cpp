// Fill out your copyright notice in the Description page of Project Settings.


#include "ScriptableParent.h"

// Sets default values
AScriptableParent::AScriptableParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AScriptableParent::BeginPlay()
{
	Super::BeginPlay();
	
}

void AScriptableParent::OnConstruction(const FTransform& Transform)
{
	StartDesigner();
	bForceOneUpdate = false;
}

void AScriptableParent::DefaultMode()
{
}

void AScriptableParent::ScriptedMode()
{
}

void AScriptableParent::ProceduralMode()
{
}

void AScriptableParent::RandomMode()
{

}

// Called every frame
void AScriptableParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AScriptableParent::StartDesigner()
{
}

void AScriptableParent::EditorFeedBack(int in)
{
	switch (in) {
	case 0: CodeStatus = "Error Caught."; break;
	case 1: CodeStatus = "Default Mode."; break;
	case 2: CodeStatus = "ScriptedMode."; break;
	default: break;
	}
}

