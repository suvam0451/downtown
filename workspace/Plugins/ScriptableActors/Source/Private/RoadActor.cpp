// Fill out your copyright notice in the Description page of Project Settings.


#include "RoadActor.h"
#include "Dependencies/Public/GameModes/ThirdP.h"
#include "Dependencies/Public/Libraries/StaticLib.h"

#define DT_PATH "/ScriptableActors/_Main/_Cached"
#define CACHE_PATH "/ScriptableActors/_Main/_Cached/"

// Sets default values
ARoadActor::ARoadActor()
{
	PrimaryActorTick.bCanEverTick = false;
	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;
	SceneRoot->SetMobility(EComponentMobility::Type::Static);

	RoadSpline = CreateDefaultSubobject<USplineComponent>(TEXT("Road spline"));
	RoadSpline->SetupAttachment(RootComponent);
}

void ARoadActor::StartDesigner()
{
	// Clean-up assets
	WW_StaticLib::ISM_RefreshByNumber(ISM_Map, 4, this, SceneRoot);
	SplineMeshCompArray.Empty();

	// Get gamemode, or create local reference
	AThirdP* GM = Cast<AThirdP>(GetWorld()->GetAuthGameMode());
	GM = GM ? GM : NewObject<AThirdP>(this);
	
	// Attempt to get the controller file from pre-defined directory
	//FAssetData DT_Asset = 
	if (GM->AssetManager->DT_LookUpAndTest<FRoad_Presets, UStaticMesh>(DT_PATH, "Road_P", _presetTable, _folderAssetData, _presetRow, Index, true) == false) { return; };
	//GM->AssetManager->FileListByClass<UStaticMesh>(CACHE_PATH + *"/" + _transitoryList[Index].ToString(), _folderAssetData);
	
	UE_LOG(LogTemp, Warning, TEXT("%s"), *_folderAssetData[0].AssetName.ToString());

	loop(i, _folderAssetData.Num()) {
		if (_folderAssetData[i].AssetName.ToString() == "RoadBase") {
			DefaultRoadMesh = Cast<UStaticMesh>(_folderAssetData[i].GetAsset());
		}
	}
	if (DefaultRoadMesh == nullptr) { UE_LOG(LogTemp, Warning, TEXT("Base for road not found. exiting.")); return; }
		


	// Declare Required variables/Acquire from tables
	FVector startLoc, endLoc, startTan, endTan;
	TArray<FVector> worldPosAlongSpline;
	int num = RoadSpline->GetNumberOfSplinePoints();
	float len = RoadSpline->GetSplineLength();
	//float segmentSize = PresetRow->segmentSize;
	float segmentSize = FMath::Max(_presetRow->BaseSegmentSize, 1.0f);	

		int iter = FMath::Max(FMath::FloorToInt(len / segmentSize), 0);
		loop(i, iter) {
			SplineMeshCompArray.Add(NewObject<USplineMeshComponent>(this));
		}
		UE_LOG(LogTemp, Warning, TEXT("Iteration number is : %d"), iter);
		loop(i, iter) {
			startLoc = RoadSpline->GetLocationAtDistanceAlongSpline(segmentSize * i, ESplineCoordinateSpace::Local);
			startTan = RoadSpline->GetTangentAtDistanceAlongSpline(segmentSize * i, ESplineCoordinateSpace::Local);
			endLoc = RoadSpline->GetLocationAtDistanceAlongSpline(segmentSize * (i + 1), ESplineCoordinateSpace::Local);
			endTan = RoadSpline->GetTangentAtDistanceAlongSpline(segmentSize * (i + 1), ESplineCoordinateSpace::Local);

			// Web API for Automatic code completion
			// For copy paste sloths :P

			SplineMeshCompArray[i]->CreationMethod = EComponentCreationMethod::UserConstructionScript;
			SplineMeshCompArray[i]->SetMobility(EComponentMobility::Type::Static);
			SplineMeshCompArray[i]->SetForwardAxis(ESplineMeshAxis::X);
			SplineMeshCompArray[i]->SetStaticMesh(DefaultRoadMesh);
			SplineMeshCompArray[i]->SetStartScale(FVector2D(1.0f, 1.0f));
			SplineMeshCompArray[i]->SetEndScale(FVector2D(1.0f, 1.0f));
			SplineMeshCompArray[i]->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
			SplineMeshCompArray[i]->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			SplineMeshCompArray[i]->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
			SplineMeshCompArray[i]->SetupAttachment(RootComponent);
			SplineMeshCompArray[i]->SetStartAndEnd(startLoc, startTan, endLoc, endTan, true);
			SplineMeshCompArray[i]->RegisterComponent();
		}
}