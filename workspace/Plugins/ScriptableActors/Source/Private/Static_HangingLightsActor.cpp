// Fill out your copyright notice in the Description page of Project Settings.


#include "Static_HangingLightsActor.h"
#include "Dependencies/Public/GameModes/ThirdP.h"
#include "Dependencies/Public/Libraries/StaticLib.h"
#include "Components/SplineComponent.h"

#define DT_PATH "/ScriptableActors/_Main/_Cached/_HangingLights"

// Sets default values
AStatic_HangingLightsActor::AStatic_HangingLightsActor()
{
	WW_Constructor;
	ParentSpline = CreateDefaultSubobject<USplineComponent>("Parent Spline");
	ParentSpline->SetupAttachment(SceneRoot);
}

void AStatic_HangingLightsActor::StartDesigner()
{
	// Get gamemode, or create local reference
	AThirdP* GM = Cast<AThirdP>(GetWorld()->GetAuthGameMode());
	GM = GM ? GM : NewObject<AThirdP>(this);

	WW_StaticLib::ISM_RefreshByNumber(ISM_Map, 1 + VariationOverrride, this, SceneRoot);

	if (GM->AssetManager->DT_LookUpAndTest<FStatic_HangingLights_Preset, UStaticMesh>(DT_PATH, "HangingLanterns_P", _presetTable, _folderAssetData, _presetRow, Index, true) == false) { EditorFeedBack(0);  return; }

	loop(i, _folderAssetData.Num()) {
		// Mendatory base cable
		if (_folderAssetData[i].AssetName.ToString() == "CableMesh") {
			ISM_Map[1]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset()));
			WireMesh = Cast<UStaticMesh>(_folderAssetData[i].GetAsset());
			//UE_LOG(LogTemp, Warning, TEXT("%d"), NumberOfSplines);
		}
		
		// Support for multiple meshes
		loop(j, VariationOverrride) {
			if (_folderAssetData[i].AssetName.ToString() == "Light" + FString::FromInt(j)) {
				ISM_Map[j + 1]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset()));
			}
		}
	}

	// Get started on appropriate mode
	bUseProceduralMode ? ProceduralMode() : (ScriptTable ? ScriptedMode() : DefaultMode());
}


void AStatic_HangingLightsActor::DefaultMode()
{
	int NumberOfSplines = ParentSpline->GetNumberOfSplinePoints() - 1;

	UE_LOG(LogTemp, Warning, TEXT("%d"), NumberOfSplines);
	float segmentSize = _presetRow->SegmentLength;

	WW_StaticLib::cleanMap(SplineMap);
	WW_StaticLib::SplineMap_RefreshByNumber(SplineMap, NumberOfSplines, this, SceneRoot);

	for (auto it : SplineMap) {
		it.Value->SetVisibility(false, true);
		it.Value->SetDrawDebug(false);
		it.Value->SetHiddenInGame(false, true);
	}

	UE_LOG(LogTemp, Warning, TEXT("%d"), SplineMap.Num());

	loop(i, NumberOfSplines) {
		FVector start = ParentSpline->GetLocationAtSplineInputKey( i, ESplineCoordinateSpace::Local);
		FVector end = ParentSpline->GetLocationAtSplineInputKey( i + 1, ESplineCoordinateSpace::Local);
		
		Spline_MC* splineMesh = WW_StaticLib::Spline_MC_Init(this, SceneRoot, WireMesh);
		splineMesh->SetStartScale(FVector2D(1, 1), true);
		splineMesh->SetEndScale(FVector2D(1, 1), true);
		FVector startTan = ParentSpline->GetTangentAtSplineInputKey(i, ESplineCoordinateSpace::Local);
		FVector endTan = ParentSpline->GetTangentAtSplineInputKey(i+1, ESplineCoordinateSpace::Local);

		splineMesh->SetStartAndEnd(start, startTan, end, endTan);

		//SplineMap.Num();
		HangValue = FMath::Max(0.0f, HangValue);
		_yVelocity = FMath::Sqrt(2 * 10.0f * HangValue);
		_xVelocity = _yVelocity * FMath::Atan(30.0f);

		_facing = FVector::VectorPlaneProject(end - start, FVector(0, 0, 1));
		_facing.ToDirectionAndLength(_unitFacing, _len);

		UE_LOG(LogTemp, Warning, TEXT("StartVector :: %f %f %f, EndVector :: %f %f %f"), start.X, start.Y, start.Z, end.X, end.Y, end.Z);

		GenerateSpline(start, end, i + 1);
		//(UpdateCables(), GenerateCurvature()) : true;	break;
	}
}

void AStatic_HangingLightsActor::ScriptedMode()
{

}

void AStatic_HangingLightsActor::ProceduralMode() {
	
}

void AStatic_HangingLightsActor::GenerateSpline(FVector startPos, FVector endPos, int index)
{
	while (SplineMap[index]->GetNumberOfSplinePoints() < Precision) \
		SplineMap[index]->AddSplinePoint(FVector(0.0f), ESplineCoordinateSpace::Local, false);
	int iterations = (SplineMap[index]->GetNumberOfSplinePoints() + 1) / 2;
	int Num = SplineMap[index]->GetNumberOfSplinePoints();
	
	loop(i, SplineMap[index]->GetNumberOfSplinePoints()-1) {
		SplineMap[index]->SetLocationAtSplinePoint(i, FVector(i * _len / (Num - 1), 0, 0) - FVector(0, 0, (_xVelocity * _xVelocity) / (2 * 10.0f)), ESplineCoordinateSpace::Local, true);
	}
	/*loop(i, iterations) {
		SplineMap[index]->SetLocationAtSplinePoint(i, startPos + ((endPos - startPos) / Num - 2) * i - (FVector(0.0f, 0.0f, HangValue) / iterations) * i, ESplineCoordinateSpace::Local, true);
		SplineMap[index]->SetLocationAtSplinePoint(Num - 1 - i, endPos - ((endPos - startPos) / Num - 2) * i - (FVector(0.0f, 0.0f, HangValue) / iterations) * i, ESplineCoordinateSpace::Local, true);
		//HangValue

		FVector startTan = SplineMap[index]->GetTangentAtSplineInputKey(i, ESplineCoordinateSpace::Local);
		FVector endTan = SplineMap[index]->GetTangentAtSplineInputKey(i, ESplineCoordinateSpace::Local);
		Spline_MC* splineMesh = WW_StaticLib::Spline_MC_Init(this, SceneRoot, WireMesh);
		splineMesh->SetStartScale(FVector2D(1,1), true);
		splineMesh->SetEndScale(FVector2D(1,1), true);
		splineMesh->SetStartAndEnd(startPos, startTan, endPos, endTan);
	}*/

}
