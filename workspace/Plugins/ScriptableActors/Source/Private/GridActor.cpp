// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GridActor.h"

//#define DT_PATH "/ScriptableActors/_Main/_Cached"
#define CACHE_PATH "/ScriptableActors/_Main/_Cached/"

// Sets default values
AGridActor::AGridActor()
{
	WW_Constructor
}

// Called to create the actor.
void AGridActor::StartDesigner() {

	// Get gamemode, or create local reference
	AThirdP* GM = Cast<AThirdP>(GetWorld()->GetAuthGameMode());
	GM = GM ? GM : NewObject<AThirdP>(this);
	
	WW_StaticLib::ISM_RefreshByNumber(ISM_Map, 3, this, SceneRoot);

	// Fetching and testing mendatory assets.
	if (GM->AssetManager->DT_LookUpAndTest<FGrid_Presets, UStaticMesh>(DT_PATH, "Grid_P", table, _folderAssetData, _PresetRow, Index, false) == false) { return; };
	//GM->AssetManager->FileListByClass<UStaticMesh>(CACHE_PATH + _transitory_list[Index].ToString(), _folderAssetData);

	// Assign assets
	loop(i, _folderAssetData.Num()) {
		(_folderAssetData[i].AssetName.ToString() == "GridBase") ? ISM_Map[1]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
		(_folderAssetData[i].AssetName.ToString() == "Pole") ? ISM_Map[2]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
		(_folderAssetData[i].AssetName.ToString() == "Railing") ? ISM_Map[3]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
	}

	// Select operation mode
	ScriptTable ? ScriptedMode() : DefaultMode();
}

void AGridActor::ScriptedMode()
{
	// Visual feedback
	EditorFeedBack(0);

	TArray<FName> Script_Rows = ScriptTable->GetRowNames();
	loop(i, Script_Rows.Num()) {
		FGrid_Script* ScriptRow = ScriptTable->FindRow<FGrid_Script>(Script_Rows[i], "", true);
	}
}

void AGridActor::DefaultMode()
{
	// Visual feedback
	EditorFeedBack(1);

	FVector start = FVector(0.0f, 0.0f, 0.0f);
	loop(i, Tiling.X) {
		loop(j, Tiling.Y) {
			FVector pos = FVector(_PresetRow->Dimensions.X * i, _PresetRow->Dimensions.Y * j, 0.0f);
			ISM_Map[1]->AddInstance(FTransform(FQuat(FVector::UpVector, 0.0f), pos, FVector(1.0f)));
		}
	}
}

