// Fill out your copyright notice in the Description page of Project Settings.


#include "FenceActor.h"
#include "Dependencies/Public/GameModes/ThirdP.h"
#include "Dependencies/Public/Libraries/StaticLib.h"

#define DT_PATH "/ScriptableActors/_Main/_Cached/_Fence"

// Sets default values
AFenceActor::AFenceActor()
{
	WW_Constructor;
}

void AFenceActor::StartDesigner() {

	// Get gamemode, or create local reference
	AThirdP* GM = Cast<AThirdP>(GetWorld()->GetAuthGameMode());
	GM = GM ? GM : NewObject<AThirdP>(this);
	
	WW_StaticLib::ISM_RefreshByNumber(ISM_Map, 2, this, SceneRoot);

	if (GM->AssetManager->DT_LookUpAndTest<FFence_Presets, UStaticMesh>(DT_PATH, "Fence_P", _presetTable, _folderAssetData, _presetRow, Index, true) == false) { EditorFeedBack(0);  return; }

	loop(i, _folderAssetData.Num()) {
		(_folderAssetData[i].AssetName.ToString() == "FenceBody") ? ISM_Map[1]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
		(_folderAssetData[i].AssetName.ToString() == "FencePost") ? ISM_Map[1]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
	}
	ScriptTable ? ScriptedMode() : DefaultMode();
}

void AFenceActor::DefaultMode()
{
	EditorFeedBack(1);
}

void AFenceActor::ScriptedMode()
{
	EditorFeedBack(2);

	FVector start = FVector(0, 0, 0);
	float CurrentRotation = 0.0f;

	TArray<FName> _scriptRowList = ScriptTable->GetRowNames();
	loop(i, _scriptRowList.Num()) {
		_scriptRow = ScriptTable->FindRow<FFence_Script>(_scriptRowList[i], "", true);
		switch (_scriptRow->Mode) {
		case 0: {
			CurrentRotation += _scriptRow->RotationChange;
			loop(j, 1) {
				ISM_Map[1]->AddInstance(FTransform(FQuat(FVector::UpVector, FMath::DegreesToRadians(CurrentRotation)), start, FVector(_scriptRow->Scale_Optional, 1.0f, 1.0f)));
			}
			start += FRotator(0, _scriptRow->RotationChange, 0).RotateVector(FVector(_presetRow->Dimensions) * _scriptRow->Scale_Optional);
			break;
		}
		case 1: {
			CurrentRotation += _scriptRow->RotationChange;
			loop(j, _scriptRow->Num_1) {
				ISM_Map[1]->AddInstance(FTransform(FQuat(FVector::UpVector, FMath::DegreesToRadians(CurrentRotation)), start, FVector(_scriptRow->Scale_Optional, 1.0f, 1.0f)));
				start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(_presetRow->Dimensions) * _scriptRow->Scale_Optional);
			}
			break;
		}
		case 2: {
			CurrentRotation += _scriptRow->RotationChange;
			start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(_scriptRow->Spacing_2_3, 0, 0));
			break;
		}
		case 3: {
		}
		default: {break; }
		}
	}
}
