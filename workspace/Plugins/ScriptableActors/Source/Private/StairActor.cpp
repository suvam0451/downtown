// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StairActor.h"
#include "Dependencies/Public/GameModes/ThirdP.h"
#include "Dependencies/Public/Libraries/StaticLib.h"

#define DT_PATH "/ScriptableActors/_Main/_Cached"
//#define CACHE_PATH "/ScriptableActors/_Main/_Cached/"

//LOCTABLE_NEW("CodeStringTable", "CodeStringTable");
//LOCTABLE_SETSTRING("CodeStringTable", "HelloWorld", "HelloWorld!");
//LOCTABLE_SETMETA("CodeStringTable", "HelloWorld", "Comment", "This is a comment about hello world");

// Sets default values
AStairActor::AStairActor()
{
	WW_Constructor
}

void AStairActor::StartDesigner() {
	UE_LOG(LogTemp, Warning, TEXT("Path is %s"), *FPaths::Combine(*(FPaths::ProjectPluginsDir()), "Shaders"));
	WW_StaticLib::ISM_RefreshByNumber(ISM_Map, 4, this, SceneRoot);

	// Get gamemode, or create local reference
	AThirdP* GM = Cast<AThirdP>(GetWorld()->GetAuthGameMode());

	// This one is a ternary assignment. Works similarly :)
	GM = GM ? GM : NewObject<AThirdP>(this);

	// This is the amcro that does the hard work , miles :).  also, #RIPparallaxoldpfp :(
	if (GM->AssetManager->DT_LookUpAndTest<FStair_Presets, UStaticMesh>(DT_PATH, "Stair_P", _presetTable, _folderAssetData, _presetRow, Index, true) == false) { EditorFeedBack(0); return; }

	loop(i, _folderAssetData.Num()) {
		(_folderAssetData[i].AssetName.ToString() == "StairBase") ? ISM_Map[1]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
		(_folderAssetData[i].AssetName.ToString() == "StairPole") ? ISM_Map[2]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
		(_folderAssetData[i].AssetName.ToString() == "StairRailing") ? ISM_Map[3]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
		(_folderAssetData[i].AssetName.ToString() == "StairPlatform") ? ISM_Map[4]->SetStaticMesh(Cast<UStaticMesh>(_folderAssetData[i].GetAsset())) : true;
	}
	UE_LOG(LogTemp, Warning, TEXT("%s"), *FText::FromStringTable("CodeStringTable", "HelloWorld").ToString());
	ScriptTable ? ScriptedMode() : DefaultMode();
}

void AStairActor::ScriptedMode()
{
	EditorFeedBack(2);
	TArray<FName> name_list = ScriptTable ? ScriptTable->GetRowNames() : TArray<FName, FDefaultAllocator>();

	// Initializing necessary variables
	FVector start = FVector(0.0f, 0.0f, 0.0f);
	float CurrentRotation = 0.0f;
	float CurrentRadians = FMath::DegreesToRadians(CurrentRotation);

	//UE_LOG(LogTemp, Warning, TEXT("Old Vector: %d"), name_list.Num());
	loop(i, name_list.Num()) {
		_scriptRow = ScriptTable->FindRow<FStair_Script>(name_list[i], "", true);
		loop(j, _scriptRow->Number) {
			if (true) {
				//start += FRotator(0, ScriptRow->RotationChange, 0).RotateVector(FVector(PresetRow->Dimensions) * ScriptRow->Scale_Optional);
				FVector vec = FVector(_presetRow->Base_Offset.X * j, 0.0f, _presetRow->Base_Offset.Z * j);
				vec = FRotator(0, CurrentRotation, 0).RotateVector(vec);
				ISM_Map[1]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start + vec, FVector(1.0f)));
			}
			if (_scriptRow->Use_Pole) {
				FVector vec1 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Pole_Offset.X, 0.0f + _presetRow->Pole_Offset.Y, _presetRow->Base_Offset.Z * j + _presetRow->Pole_Offset.Z);
				FVector vec2 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Pole_Offset.X, 0.0f - _presetRow->Pole_Offset.Y, _presetRow->Base_Offset.Z * j + _presetRow->Pole_Offset.Z);
				vec1 = FRotator(0, CurrentRotation, 0).RotateVector(vec1);
				vec2 = FRotator(0, CurrentRotation, 0).RotateVector(vec2);
				j% _presetRow->PoleInterval == 0 ? \
					(Use_Left ? ISM_Map[2]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start + vec1, FVector(1.0f))) : true), \
					(Use_Right ? ISM_Map[2]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start + vec2, FVector(1.0f))) : true) : true;
			}
			if (_scriptRow->Use_Railing) {
				FVector vec1 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Railing_Offset.X, 0.0f + _presetRow->Railing_Offset.Y, _presetRow->Railing_Offset.Z * j + _presetRow->Railing_Offset.Z);
				FVector vec2 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Railing_Offset.X, 0.0f - _presetRow->Railing_Offset.Y, _presetRow->Base_Offset.Z * j + _presetRow->Railing_Offset.Z);
				vec1 = FRotator(0, CurrentRotation, 0).RotateVector(vec1);
				vec2 = FRotator(0, CurrentRotation, 0).RotateVector(vec2);
				j% _presetRow->PoleInterval == 0 ? \
					(Use_Left ? ISM_Map[3]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start + vec1, FVector(1.0f))) : true), \
					(Use_Right ? ISM_Map[3]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start + vec2, FVector(1.0f))) : true) : true;
			}
		}
		//UE_LOG(LogTemp, Warning, TEXT("CurrentRotation is : %f"), CurrentRotation);
		//UE_LOG(LogTemp, Warning, TEXT("start vector old: %f %f %f"), start.X, start.Y, start.Z);
		start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(_presetRow->Base_Offset.X * _scriptRow->Number, 0, _presetRow->Base_Offset.Z * _scriptRow->Number));
		//UE_LOG(LogTemp, Warning, TEXT("start vector new: %f %f %f"), start.X, start.Y, start.Z);

		switch (_scriptRow->NextRotation) {
		case EStair_Turns::Forward: {
			if (_scriptRow->Use_UpperBase) {
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start, FVector(1.0f)));
				start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(_presetRow->Platform_Dim.X, 0, 0));
			}
			break;
		}
		case EStair_Turns::RightAboutTurn: {
			if (_scriptRow->Use_UpperBase) {
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start, FVector(1.0f)));
				start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(0, _presetRow->Platform_Dim.Y, 0));
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians + 180.0f), start, FVector(1.0f)));
			}
			CurrentRotation += 180.0f;
			CurrentRadians = FMath::DegreesToRadians(CurrentRotation);
			break;
		}
		case EStair_Turns::LeftAboutTurn: {
			if (_scriptRow->Use_UpperBase) {
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start, FVector(1.0f)));
				start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(0, _presetRow->Platform_Dim.Y, 0));
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start, FVector(1.0f)));
			}
			CurrentRotation += 180.0f;
			CurrentRadians = FMath::DegreesToRadians(CurrentRotation);
			break;
		}
		case EStair_Turns::LeftTurn: {
			if (_scriptRow->Use_UpperBase) {
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start, FVector(1.0f)));
				start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(_presetRow->Platform_Dim.X / 2, _presetRow->Platform_Dim.Y / 2, 0));
			}
			CurrentRotation += 90.0f;
			//UE_LOG(LogTemp, Warning, TEXT("CurrentRotation is : %f"), CurrentRotation);
			CurrentRadians = FMath::DegreesToRadians(CurrentRotation);
			break;
		}
		case EStair_Turns::RightTurn: {
			if (_scriptRow->Use_UpperBase) {
				ISM_Map[4]->AddInstance(FTransform(FQuat(FVector::UpVector, CurrentRadians), start, FVector(1.0f)));
				start += FRotator(0, CurrentRotation, 0).RotateVector(FVector(_presetRow->Platform_Dim.X / 2, _presetRow->Platform_Dim.Y / 2, 0));
			}
			CurrentRotation -= 90.0f;
			CurrentRadians = FMath::DegreesToRadians(CurrentRotation);
			break;
		}
		default: {break; }
		}
		//UE_LOG(LogTemp, Warning, TEXT("start vector new new: %f %f %f"), start.X, start.Y, start.Z);
	}
}

void AStairActor::DefaultMode()
{
	// Give feedback to user
	EditorFeedBack(1);
	//UE_LOG(LogTemp, Warning, TEXT("Pole offset is %f %f %f"), PresetRow->Pole_Offset.X, PresetRow->Pole_Offset.Y, PresetRow->Pole_Offset.Z);
	loop(j, StairNum) {
		if (Use_Base) {
			FVector vec = FVector(_presetRow->Base_Offset.X * j, 0.0f, _presetRow->Base_Offset.Z * j);
			ISM_Map[1]->AddInstance(FTransform(FQuat(FVector::UpVector, 0.0f), vec, FVector(1.0f)));
		}
		if (Use_Pole) {
			FVector vec1 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Pole_Offset.X, 0.0f + _presetRow->Pole_Offset.Y, _presetRow->Base_Offset.Z * j + _presetRow->Pole_Offset.Z);
			FVector vec2 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Pole_Offset.X, 0.0f - _presetRow->Pole_Offset.Y, _presetRow->Base_Offset.Z * j + _presetRow->Pole_Offset.Z);
			//UE_LOG(LogTemp, Warning, TEXT("Pole offset is %f %f %f"), vec1.X, vec1.Y, vec1.Z);
			j % _presetRow->PoleInterval == 0 ? \
				(Use_Left ? ISM_Map[2]->AddInstance(FTransform(FQuat(FVector::UpVector, 0.0f), vec1, FVector(1.0f))) : true), \
				(Use_Right ? ISM_Map[2]->AddInstance(FTransform(FQuat(FVector::UpVector, 0.0f), vec2, FVector(1.0f))) : true) : true;
		}
		if (Use_Railing) {
			FVector vec1 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Railing_Offset.X, 0.0f + _presetRow->Railing_Offset.Y, _presetRow->Railing_Offset.Z * j + _presetRow->Railing_Offset.Z);
			FVector vec2 = FVector(_presetRow->Base_Offset.X * j + _presetRow->Railing_Offset.X, 0.0f - _presetRow->Railing_Offset.Y, _presetRow->Base_Offset.Z * j + _presetRow->Railing_Offset.Z);
			if (j % _presetRow->PoleInterval == 0) {
				if (Use_Left) {
					ISM_Map[3]->AddInstance(FTransform(FQuat(FVector::UpVector, 0.0f), vec1, FVector(1.0f)));
				}
				if (Use_Right) {
					ISM_Map[3]->AddInstance(FTransform(FQuat(FVector::UpVector, 0.0f), vec2, FVector(1.0f)));
				}
			}
		}
	}
}