// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "IScriptableActors.h"
#include "Modules/ModuleManager.h"

class FScriptableActors : public IScriptableActors
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

IMPLEMENT_MODULE(FScriptableActors, ScriptableActors)

void FScriptableActors::StartupModule() {}
void FScriptableActors::ShutdownModule() {}




