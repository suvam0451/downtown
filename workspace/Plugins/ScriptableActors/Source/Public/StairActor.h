// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "Misc/Paths.h"
#include "StairActor.generated.h"

UCLASS()
class SCRIPTABLEACTORS_API AStairActor : public AScriptableParent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AStairActor();

	// Number of stairs for default operation mode
	UPROPERTY(EditAnywhere, Category = "Scriptables Extended")
		int StairNum;

	// Boolean paramters
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Base = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Pole = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Railing = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Left = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Right = true;

private:

	// override used functions only
	virtual void StartDesigner() override;

	// private tables/rows/references assets
	FStair_Presets* _presetRow;
	FStair_Script* _scriptRow;


	// ------------------------------ //
	// Actor specific implementations //
	// ------------------------------ //
	
	// Other actor-specific functions
	virtual void DefaultMode() override;
	virtual void ScriptedMode() override;

	// Other actor-specific assets
	UStaticMesh* DefaultRoadMesh = nullptr;

	// Other actor-specific containers
	TMap<int, FString> StringTable;
};
