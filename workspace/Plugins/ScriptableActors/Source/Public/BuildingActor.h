// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "BuildingActor.generated.h"

USTRUCT(BlueprintType)
struct FWall_PositionData : public FTableRowBase
{
	GENERATED_BODY()
public:
	/** montage **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int StartIndex;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int EndIndex;
	/** count of animations in montage **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Z_Rotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Compensation;
	/** description **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Asset_Index;
};

USTRUCT(BlueprintType)
struct FWall_ObjectData : public FTableRowBase
{
	GENERATED_BODY()
public:
	/** montage **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Index;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int EndIndex;
	/** count of animations in montage **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Z_Rotation;
	/** description **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMesh* wallMesh;
};

USTRUCT(BlueprintType)
struct FWall_Actor_Out : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSoftClassPtr<AActor> MyClass;
};

USTRUCT(BlueprintType)
struct FWall_Actor_In : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSoftClassPtr<AActor> MyClass;
};

USTRUCT(BlueprintType)
struct FWall_Actor_On : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSoftClassPtr<AActor> MyClass;
};

UCLASS()
class SCRIPTABLEACTORS_API ABuildingActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ABuildingActor();
	UPROPERTY(EditAnywhere)
		bool bForceOneUpdate;

	UPROPERTY(VisibleAnywhere)
		bool bParsedLocationArray = false;
	UPROPERTY(VisibleAnywhere)
		bool bFoundObjectArray = false;

	// Advanced Component settings
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool bUseInnerActor = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool bUseOuterActor = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool bUseOnActor = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool bUseInActor = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool Use_Right = true;
private:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnConstruction(const FTransform& Transform) override;
	void StartDesigner();
	UPROPERTY()
		USceneComponent* SceneRoot;
};
