 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "SplineBridge.generated.h"

UCLASS()
class SCRIPTABLEACTORS_API ASplineBridge : public AScriptableParent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplineBridge();

	// Number of stairs for default operation mode
	UPROPERTY(EditAnywhere, Category = "Param")
		int StairNum;

	/* Additional exposed components */
	UPROPERTY(EditAnywhere, Category = "Param")
		USplineComponent* BridgeSpline;

private:

	// private tables/rows/references assets
	FSplineBridge_Presets* _presetRow;
	FSplineBridge_Script* _scriptRow;

	// Other actor-specific assets
	UStaticMesh* DefaultRoadMesh = nullptr;

	virtual void StartDesigner() override;
};
