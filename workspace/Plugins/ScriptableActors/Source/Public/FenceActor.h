// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "FenceActor.generated.h"

UCLASS()
class SCRIPTABLEACTORS_API AFenceActor : public AScriptableParent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFenceActor();

private:

	// Required Functions
	virtual void StartDesigner() override;

	// private tables/rows/references assets
	FFence_Presets* _presetRow;
	FFence_Script* _scriptRow;

	// Other actor-specific functions
	virtual void DefaultMode() override;
	virtual void ScriptedMode() override;

	UStaticMesh* Fence_Mesh = nullptr;
};
