// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TableTemplates.generated.h"

// Remove if not included in plug-in
#define Stairs 0
#define SplineBridge 1

/*
	-------------------------------------
	SECTION -- STAIR SCRIPT
	-------------------------------------
*/

UENUM(BlueprintType)
enum class EStair_Turns : uint8
{
	Forward UMETA(DisplayName = "Continue forward"),
	LeftAboutTurn UMETA(DisplayName = "180 turn - turning left"),
	RightAboutTurn UMETA(DisplayName = "180 turn - turning left"),
	LeftTurn UMETA(DisplayName = "Turn left"),
	RightTurn UMETA(DisplayName = "Turn right"),
};

USTRUCT(BlueprintType)
struct FStair_Script : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		int Number;
	UPROPERTY(EditAnywhere)
		EStair_Turns NextRotation;
	UPROPERTY(EditAnywhere)
		bool Use_Pole = true;
	UPROPERTY(EditAnywhere)
		bool Use_Railing = true;
	UPROPERTY(EditAnywhere)
		bool Use_Left = true;
	UPROPERTY(EditAnywhere)
		bool Use_Right = true;
	UPROPERTY(EditAnywhere)
		bool Use_LowerBase = true;
	UPROPERTY(EditAnywhere)
		bool Use_UpperBase = true;
};

USTRUCT(BlueprintType)
struct FStair_Presets : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		FVector Base_Offset;
	UPROPERTY(EditAnywhere)
		FVector Pole_Offset;
	UPROPERTY(EditAnywhere)
		FVector Railing_Offset;
	UPROPERTY(EditAnywhere)
		FVector Platform_Dim;
	UPROPERTY(EditAnywhere)
		int PoleInterval = 2;
	UPROPERTY(EditAnywhere)
		int StairVariation = 1;
};

/*
	-------------------------------------
	SECTION -- SPLINE BASED BRIDGE SCRIPT
	-------------------------------------
*/

// Table Templates
USTRUCT(BlueprintType)
struct FSplineBridge_Script : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		int Number;
	UPROPERTY(EditAnywhere)
		EStair_Turns NextRotation;
	UPROPERTY(EditAnywhere)
		bool Use_Pole = true;
	UPROPERTY(EditAnywhere)
		bool Use_Railing = true;
	UPROPERTY(EditAnywhere)
		bool Use_Left = true;
	UPROPERTY(EditAnywhere)
		bool Use_Right = true;
	UPROPERTY(EditAnywhere)
		bool Use_LowerBase = true;
	UPROPERTY(EditAnywhere)
		bool Use_UpperBase = true;
};

USTRUCT(BlueprintType)
struct FSplineBridge_Presets : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		FVector Base_Offset;
	UPROPERTY(EditAnywhere)
		FVector Pole_Offset;
	UPROPERTY(EditAnywhere)
		FVector Railing_Offset;
	UPROPERTY(EditAnywhere)
		FVector Platform_Dim;
	UPROPERTY(EditAnywhere)
		int PoleInterval = 2;
	UPROPERTY(EditAnywhere)
		int StairVariation = 1;
};

/*
	-------------------------------------
	SECTION -- ROAD SCRIPT
	-------------------------------------
*/

USTRUCT(BlueprintType)
struct FRoad_Presets : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		float BaseSegmentSize;
	UPROPERTY(EditAnywhere)
		int SidePost_Interval = 2;
	UPROPERTY(EditAnywhere)
		FVector SidePost_Offset;

	UPROPERTY(EditAnywhere)
		float SideProp_SegmentSize;
	UPROPERTY(EditAnywhere)
		FVector SideProp_Offset;

	UPROPERTY(EditAnywhere)
		int Initial = 1;
};

USTRUCT(BlueprintType)
struct FRoad_Script : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		float BaseSegmentSize;
	UPROPERTY(EditAnywhere)
		int SidePost_Interval = 2;
	UPROPERTY(EditAnywhere)
		FVector SidePost_Offset;

	UPROPERTY(EditAnywhere)
		float SideProp_SegmentSize;
	UPROPERTY(EditAnywhere)
		FVector SideProp_Offset;

	UPROPERTY(EditAnywhere)
		int Initial = 1;
};

/*
	-------------------------------------
	START SECTION -- GRID SCRIPT
	-------------------------------------
*/

USTRUCT(BlueprintType)
struct FGrid_Presets : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	// Dimensions of grid piece as set by artist
	UPROPERTY(EditAnywhere)
		FVector Dimensions = FVector(375, 375, 25);

	// Default tiling, a quick implementation for designer. defaults to 1*1*1.
	UPROPERTY(EditAnywhere)
		FVector Default_Tiling = FVector(1, 1, 1);
};

USTRUCT(BlueprintType)
struct FGrid_Script : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		FString BinaryInput;
};

/*
	-------------------------------------
	END SECTION -- GRID SCRIPT
	-------------------------------------
*/

/*
	-------------------------------------
	START SECTION -- FENCE SCRIPT
	-------------------------------------
*/
USTRUCT(BlueprintType)
struct FFence_Presets : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		FVector Dimensions;
};

USTRUCT(BlueprintType)
struct FFence_Script : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		float RotationChange;
	UPROPERTY(EditAnywhere)
		int Mode;
	UPROPERTY(EditAnywhere)
		int Num_1;
	UPROPERTY(EditAnywhere)
		float Scale_Optional = 1.0f;
	UPROPERTY(EditAnywhere)
		float Spacing_2_3;
	UPROPERTY(EditAnywhere)
		FString FileName_3;
};

/*
	-------------------------------------
	END SECTION -- FENCE SCRIPT
	-------------------------------------
*/

/*
	-------------------------------------
	START SECTION -- HANGING LIGHTS SCRIPT
	-------------------------------------
*/

USTRUCT(BlueprintType)
struct FStatic_HangingLights_Preset : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		float SegmentLength = 100.0f;
	UPROPERTY(EditAnywhere)
		float Prescribed_LightSegments = 10.0f;
};

USTRUCT(BlueprintType)
struct FStatic_HangingLights_Script : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		int VariationSeed = 1;
	UPROPERTY(EditAnywhere)
		int UseOverride;
	UPROPERTY(EditAnywhere)
		int OverrideSize = 1;
	UPROPERTY(EditAnywhere)
		FString Override_Param;
};

/*
	-------------------------------------
	END SECTION -- HANGING LIGHTS SCRIPT
	-------------------------------------
*/

UCLASS()
class SCRIPTABLEACTORS_API UTableTemplates : public UObject
{
	GENERATED_BODY()
	
};
