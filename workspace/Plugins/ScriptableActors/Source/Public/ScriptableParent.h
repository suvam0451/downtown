// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Components/SplineComponent.h"
#include "TableTemplates.h"
#include "Components/SplineMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Dependencies/Public/GameModes/ThirdP.h"
#include "Dependencies/Public/Libraries/StaticLib.h"
#include "Runtime/Core/Public/Internationalization/StringTableRegistry.h"
#include "ScriptableParent.generated.h"

UCLASS()
class SCRIPTABLEACTORS_API AScriptableParent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AScriptableParent();

	UPROPERTY(EditAnywhere, Category = "Params_Base")
		int Index;
	UPROPERTY(EditAnywhere, Category = "Params_Base")
		bool bForceOneUpdate;

	/* Plug-in feedback for level designer
	List of available modes of operation
	@ Default mode - basic functions.
	@ Scripted mode - Use a script to rotate
	*/
	UPROPERTY(VisibleAnywhere, Category = "Params_Base")
		FString CodeStatus = "Not initialized yet";

	// Optionally a data table for scripted input
	UPROPERTY(EditAnywhere, Category = "Params_Base")
		UDataTable* ScriptTable;

	// ReadOnly Mode display (4 modes implemented)
	UPROPERTY(VisibleAnywhere, Category = "Params_Base")
		bool bUsingDefaultMode = true;
	UPROPERTY(VisibleAnywhere, Category = "Params_Base")
		bool bUsingScriptedMode = false;
	UPROPERTY(VisibleAnywhere, Category = "Params_Base")
		bool bUsingProceduralMode = false;
	UPROPERTY(VisibleAnywhere, Category = "Params_Base")
		bool bUsingRandomMode = false;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Params_Base")
		bool bUseProceduralMode = false;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Params_Base")
		bool bUseRandomMode = false;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	// Calls StartDesigner() and sets bForceOneUpdate to false.
	virtual void OnConstruction(const FTransform& Transform) override;

	// For printing in editor interface
	virtual void EditorFeedBack(int in);

	// Called to manually start construction of actor
	virtual void StartDesigner();

	// Map to store all our ISM components
	TMap<int, UInstancedStaticMeshComponent*> ISM_Map;

	// This one is pretty unnecessary.
	TArray<FName> _transitoryList;
	// Holds information about all the assets found in a folder.
	TArray<FAssetData> _folderAssetData;


	/* Functions for various modes offered */
	/* Activated by default if no script is given */
	virtual void DefaultMode();
	/* Activated, only if  avalid script is detected. */
	virtual void ScriptedMode();
	/* Activated, only if  Procedural Mode is explicitly requested. */
	virtual void ProceduralMode();
	/* Activated, only if  Random mode is explicitly requested. */
	virtual void RandomMode();

	// Our own scene component, because why not ?
	USceneComponent* SceneRoot;

	// Used to store assetregistry obtained table intermediately.
	UDataTable* table;

	// An optional preset table.
	UDataTable* _presetTable;
};
