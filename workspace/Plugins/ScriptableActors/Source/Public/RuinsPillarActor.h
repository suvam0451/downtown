// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "Dependencies/Public/Libraries/StaticLib.h"
#include "RuinsPillarActor.generated.h"

USTRUCT(BlueprintType)
struct FRuinPillars_Presets : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere)
		float PillarSectionHeight = 100.0f;
	UPROPERTY(EditAnywhere)
		FVector CapDimensions = FVector(10.0f, 10.0f, 10.0f);
	UPROPERTY(EditAnywhere)
		float OptimalSpacing = 100.0f;
	UPROPERTY(EditAnywhere)
		int BaseAvailable = 1;
	UPROPERTY(EditAnywhere)
		int BodiesAvailable = 1;
	UPROPERTY(EditAnywhere)
		int CapsAvailable = 1;
	UPROPERTY(EditAnywhere)
		int BrokenCapsAvailable = 1;
};


UCLASS()
class SCRIPTABLEACTORS_API ARuinsPillarActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARuinsPillarActor();

	UPROPERTY(EditAnywhere, Category = "Param")
		FString PillarHeight;
	UPROPERTY(EditAnywhere, Category = "Param")
		FString UsePillarCeiling;

	//UPROPERTY(EditAnywhere, Category = "Param")
	//	bool bUseRandomCeiling;
	//UPROPERTY(EditAnywhere, Category = "Param")
	//	bool bUseRandomBody;

	// Debug information
	UPROPERTY(VisibleAnywhere, Category = "Param")
		bool bUsingRandomCeiling = false;
	UPROPERTY(VisibleAnywhere, Category = "Param")
		bool bUsingRandomBody = false;

	// Advanced Display section
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool bUseRandomCeiling = false;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Param")
		bool bUseRadomBody = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	TMap<int, FVector> LocationArray;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
