// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "RoadActor.generated.h"

UCLASS(BlueprintType)
class SCRIPTABLEACTORS_API ARoadActor : public AScriptableParent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoadActor();

	UPROPERTY(EditAnywhere, Category = "Params_Unique")
		UStaticMesh* RoadMesh;
	UPROPERTY(EditAnywhere, Category = "Params_Unique")
		float SpacingInput = 0.0f;

	// Optionally a data table for scripted input
	UPROPERTY(EditAnywhere, Category = "Params_Unique")
		USplineComponent* RoadSpline;
	UPROPERTY(EditAnywhere, Category = "Params_Unique")
		USplineComponent* RoadCrossSection;
	UPROPERTY(EditAnywhere, Category = "Params_Unique")
		TArray<USplineMeshComponent*> SplineMeshCompArray;
	// Debug parameters

protected:

	// Derived functions as required
	virtual void StartDesigner() override;
	//virtual void DefaultMode() override;
	//virtual void ScriptedMode() override;

	FRoad_Presets* _presetRow;
	FRoad_Script* _scriptRow;

	UStaticMesh* DefaultRoadMesh = nullptr;
};
