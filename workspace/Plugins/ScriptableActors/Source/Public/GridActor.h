// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "GridActor.generated.h"

UCLASS()
class SCRIPTABLEACTORS_API AGridActor : public AScriptableParent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actors properties
	AGridActor();

	UPROPERTY(EditAnywhere, Category = "Param")
		FIntVector Tiling = FIntVector(1,1,0);

private:
	virtual void StartDesigner() override;

	
	FGrid_Presets* _PresetRow;
	FGrid_Script* _ScriptRow;
	
	virtual void ScriptedMode() override;
	virtual void DefaultMode() override;
};
