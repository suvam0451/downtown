// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "Static_HangingLightsActor.generated.h"

UCLASS()
class SCRIPTABLEACTORS_API AStatic_HangingLightsActor : public AScriptableParent
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AStatic_HangingLightsActor();

	UPROPERTY(EditAnywhere, Category = "Param")
		float HangValue;
	UPROPERTY(EditAnywhere, Category = "Param")
		int Precision = 6;

	UPROPERTY(EditAnywhere, Category = "Param")
		int VariationOverrride = 1;

	UPROPERTY(EditAnywhere, Category = "Param")
		USplineComponent* ParentSpline;

private:
	// ------------------------------ //
	// -Scriptables implementations-- //
	// ------------------------------ //

	// Required Functions
	virtual void StartDesigner() override;

	FStatic_HangingLights_Preset* _presetRow;
	FStatic_HangingLights_Script* _scriptRow;

	// ------------------------------ //
	// Actor specific implementations //
	// ------------------------------ //


	virtual void DefaultMode() override;
	virtual void ScriptedMode() override;
	virtual void ProceduralMode() override;

	void GenerateCurvature();
	void UpdateCables();
	void GenerateSpline(FVector start, FVector end,  int index);

	// Other actor-specific assets
	UStaticMesh* WireMesh = nullptr;
	float _xVelocity;
	float _yVelocity;
	float _simulationTime;

	FVector _unitFacing;
	float _len;
	FVector _facing;

	TMap<int, USplineComponent*> SplineMap;

	UPROPERTY()
		UDataTable* P_Table;
	UPROPERTY()
		UStaticMesh* Fence_Mesh = nullptr;
};
